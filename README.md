To Run the project

Do 

1) Install NodeJs using Command Prompt/Terminal <br />
2) Pull the repository, navigate into folder <br />
3) Do install following in NodJs <br />
	i) npm install <br />
	ii) npm install --save-dev lite-server <br />

		Copy & Paste the below code to the generated “package.json".

		{
		  "name": "TMS",
		  "version": "1.0.0",
		  "description": "smit",
		  "main": "index.js",
		  "scripts": {
		    "start": "lite-server",
		    "scss": "node-sass --watch lite/scss -o lite/css"
		  },
		  "repository": {
		    "type": "git",
		    "url": "git+https://gitlab.com/mail4anish.k/TMS.git"
		  },
		  "keywords": [
		    "SMIT"
		  ],
		  "author": "anish",
		  "license": "ISC",
		  "bugs": {
		    "url": "https://gitlab.com/mail4anish.k/TMS/issues"
		  },
		  "homepage": "https://gitlab.com/mail4anish.k/TMS#README",
		  "devDependencies": {
		    "lite-server": "^2.3.0"
		  },
		  "dependencies": {
		    "node-sass": "^4.7.2"
		  }
		}

4) npm start - to run the application <br />
5) npm run scss - to compile css from sass
