// SMIT TMS App

var app = angular.module("smitApp", []);
app.controller("ListController", ['$scope', function($scope) {
    $scope.taskDetails = [
        {
            'assignee':'Anish K',
            'reporter':'Omar',
            'taskType':'UI/ UX',
            'id':'0'
        },
        {
            'assignee':'Anish K',
            'reporter':'Venkat',
            'taskType':'UX',
            'id':'1'
        },
        {
            'assignee':'Anish K',
            'reporter':'Omar',
            'taskType':'UI',
            'id':'2'
        }
    ];
    $scope.addNew = function(){     
        $scope.taskDetails.push({ 'assignee':$scope.assignee, 'reporter': $scope.reporter, 'taskType':$scope.taskType });
        $scope.assignee='';
        $scope.reporter='';
        $scope.taskType='';
        $('#addTask').modal('hide');
    };

    $scope.showAddModal = function(taskDetails){
        $('#addTask').modal('show');
    };
    
    $scope.viewData = function(taskData){
        $scope.userData = taskData;
        $('#viewTask').modal('show');
        
    };

    $scope.remove = function(){
        var newDataList=[];
        $scope.selectedAll = false;
        angular.forEach($scope.taskDetails, function(selected){
            if(!selected.selected){
                newDataList.push(selected);
            }
        }); 
        $scope.taskDetails = newDataList;
        $('.tooltip').removeClass('show')
    };

    $scope.checkAll = function () {
        if (!$scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.taskDetails, function(taskDetails) {
            taskDetails.selected = $scope.selectedAll;
        });
    }; 

    $scope.editTD = {};
    
    for (var i = 0, length = $scope.taskDetails.length; i < length; i++) {
      $scope.editTD[$scope.taskDetails[i].id] = false;
    }


    $scope.modify = function(taskDetails){
        $scope.editTD[taskDetails.id] = true;
    };


    $scope.update = function(taskDetails){
        $scope.editTD[taskDetails.id] = false;
    };
    
}]); 